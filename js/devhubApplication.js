var app = angular.module('devhubApp', ['ngRoute', 'ngCookies']);

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
	$routeProvider
	.when('/', {
		templateUrl: 'main.html'
	})
	.otherwise({
		redirectTo: '/'
	});
	
	$locationProvider.html5Mode(true);
}]);

app.run(function($rootScope){
	$rootScope.currentUser = null;
});

app.controller('loginController', function($scope, $rootScope, $location, $http, $window, $cookies)
{
	$scope.authorizationFailed = false;
	$scope.user = {};
	$scope.authentificateUser = function(){
		
		console.log("loginController.submit");
		console.log("User Data: " + JSON.stringify($scope.user));
		
		var loginUrl = "/server/users/getUserByMailAndPassword.php";
		
		$http.post(loginUrl, $scope.user)
		.success(
			function(data, status, headers){
				console.log("Data: " + JSON.stringify(data));
				console.log("Status: " + status);
				console.log("Headers: " + headers);
				
				if(data != "" && data != null){
					$rootScope.currentUser = data;
					
					//Set obtained user data into cookies with expired Date
					var expireDate = new Date();
					expireDate.setDate(expireDate.getDate() + 1);
					$cookies.putObject("currentUser", data, {'expires': expireDate});
					$window.location.href = '/';
					//$window.location.href = '/dashboard/dashboard.html';
				} else {
					$scope.authorizationFailed = true;
				}
			}
		)
		.error(
			function(data, status, headers){
				console.log("Data: " + data);
				console.log("Status: " + status);
				console.log("Headers: " + headers);
			}
		);
	}
});

app.controller('registrationController', function($scope, $location, $http, $window){
	$scope.user = {};
	$scope.serverAnswer = -1;
	$scope.RegisterNewUser = function(){
		
		$scope.user["isResident"] = 0;
		$scope.user["isUserAdmin"] = 0;
		$scope.user["usedTestDay"] = 0;
		$scope.user["registrationDate"] = Date.now();
		
		console.log("registrationController.RegisterNewUser");
		console.log("User Data: " + JSON.stringify($scope.user));
		
		var registrationUrl = "/server/users/createNewUser.php";
		
		$http.post(registrationUrl, $scope.user)
		.success(
			function(data, status, headers){
				console.log("Data: " + data);
				console.log("Status: " + status);
				console.log("Headers: " + headers);
				
				$scope.serverAnswer = parseInt(data);
			}
		)
		.error(
			function(data, status, headers){
				console.log("Data: " + data);
				console.log("Status: " + status);
				console.log("Headers: " + headers);
			}
		);
	}
});

app.controller('logoutContoller', function($scope, $rootScope, $window, $cookies){
	
	if($cookies.getObject("currentUser") != null) {
		$scope.user = $cookies.getObject("currentUser");
	}
	
	$scope.Logout = function(){
		if($cookies.getObject("currentUser") != null) {
			$cookies.remove("currentUser");
		}
		$scope.user = null;
		$window.location.href = '/';
	}
	
	$scope.Dashboard = function(){
		$window.location.href = '/dashboard/dashboard.html';
	}
});