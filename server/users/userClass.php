<?php
	class User
	{
		public $userID;
		public $userPass;
		public $firstName;
		public $lastName;
		public $userMail;
		public $userGender;
		public $userMobile;
		public $isStudent;
		public $isResident;
		public $isUserAdmin;
		public $usedTestDay;
		public $registrationDate;
		
		public function __construct($a_ID, $a_FirstName, $a_LastName, $a_UserMobile, $a_UserMail, $a_UserPass, $a_userGender, $a_IsStudent, $a_IsResudet, $a_IsAdmin, $a_UsedTestDay, $a_RegDate)
		{
			$this->userID = $a_ID;
			$this->userPass = $a_UserPass;
			$this->firstName = $a_FirstName;
			$this->lastName = $a_LastName;
			$this->userMobile = $a_UserMobile;
			$this->userMail = $a_UserMail;
			$this->userGender = $a_userGender;
			$this->isStudent = $a_IsStudent;
			$this->isResident = $a_IsResudet;
			$this->isUserAdmin = $a_IsAdmin;
			$this->usedTestDay = $a_UsedTestDay;
			$this->registrationDate = $a_RegDate;
		}
	}
?>