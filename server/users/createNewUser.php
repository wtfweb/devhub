<?php	

	//User registered successful - 0
	//User with this e-mail is already registered - 1
	//User with this mobile is already registered - 2
	
	//Create new User in DB
	//Include Config
	require ("../config.php");
	require ("userClass.php");
    
    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
    
	if ($mysqli->connect_error) {
		die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
	}
	
	if (mysqli_connect_error()) {
		die('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
	}
	
	function CreateNewUser($connection, $newUser)
	{	
		$connection->set_charset("utf8");
		
		$query = "INSERT INTO users (
			firstName,
			lastName,
			mobile,
			email,
			password,
			isStudent,
			isResident,
			isUserAdmin,
			usedTestDay,
			registrationDate,
			gender
		) VALUES (".
			"\"" . $newUser->firstName . "\"," .
			"\"" . $newUser->lastName . "\"," .
			"\"" . $newUser->userMobile . "\"," .
			"\"" . $newUser->userMail . "\"," .
			"\"" . $newUser->userPass . "\"," .
			$newUser->isStudent . "," .
			$newUser->isResident . "," .
			$newUser->isUserAdmin . "," .
			$newUser->usedTestDay . "," .
			"\"" . MillisecondsToFormatedDate($newUser->registrationDate) . "\"," .
			$newUser->userGender . ")";
			
		$connection->query($query);
	}
	
	function MillisecondsToFormatedDate($a_MillisecondsDate){
		$seconds = $a_MillisecondsDate / 1000;
		return date("Y-m-d H:i:s", $seconds);
	}
	
	function CheckUserEmail($connection, $mail) {
		if ($result = $connection->query("SELECT * FROM users WHERE email='". $mail . "'")) {
			
			while ($user = $result->fetch_object())
			{				
				return true;
			}
						
		    $result->close();
		}
	}
	
	function CheckUserMobile($connection, $mobile) {
		if ($result = $connection->query("SELECT * FROM users WHERE mobile='". $mobile . "'")) {
			
			while ($user = $result->fetch_object())
			{				
				return true;
			}
						
		    $result->close();
		}
	}

	$userData = json_decode(file_get_contents('php://input'), true);
	
	if(isset($userData) && !is_null($userData) && !empty($userData)){
		
		$newUser = new User(
			null,
			$userData["firtsName"],
			$userData["lastName"],
			$userData["mobile"],
			$userData["email"],
			$userData["password"],
			$userData["gender"],
			$userData["isStudent"],
			$userData["isResident"],
			$userData["isUserAdmin"],
			$userData["usedTestDay"],
			$userData["registrationDate"]
		);
		
		$checkSum = 0;
		
		if(CheckUserEmail($mysqli, $newUser->userMail)) {
			$checkSum += 1;
		}
		
		if(CheckUserEmail($mysqli, $newUser->userMail)) {
			$checkSum += 2;
		}
		
		if($checkSum == 0){
			CreateNewUser($mysqli, $newUser);
		}
		
		echo $checkSum;
	} else {
		die("User data was not passed properly to POST!");
	}
    
    $mysqli->close();
	
?>