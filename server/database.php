<?php	
	
	//Include Config
	require ("config.php");
    
    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
    
	if ($mysqli->connect_error) {
		die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
	}
	
	if (mysqli_connect_error()) {
		die('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
	}
	
	class User
	{
		public $userID;
		public $userPass;
		public $firstName;
		public $lastName;
		public $userMobile;
		public $userMail;
		public $isStudent;
		public $isResident;
		public $isUserAdmin;
		public $usedTestDay;
		public $registrationDate;
		
		public function __construct($a_ID, $a_UserPass, $a_FirstName, $a_LastName, $a_UserMobile, $a_UserMail, $a_IsStudent, $a_IsResudet, $a_IsAdmin, $a_UsedTestDay, $a_RegDate)
		{
			$this->userID = $a_ID;
			$this->userPass = $a_UserPass;
			$this->firstName = $a_UserName;
			$this->lastName = $a_LastName;
			$this->userMobile = $a_UserMobile;
			$this->userMail = $a_UserMail;
			$this->isStudent = $a_IsStudent;
			$this->isResident = $a_IsResudet;
			$this->isUserAdmin = $a_IsAdmin;
			$this->usedTestDay = $a_UsedTestDay;
			$this->registrationDate = $a_RegDate;
		}
	}
	
	//Get All Users
	function GetAllUsers($connection)
	{
		if ($result = $connection->query("SELECT * FROM users")) {
			
			$funalStart = '{"users":[';
			
			$usersCount = $result->num_rows;
			$i = 0;
			
			echo($usersCount);
			
			while ($user = $result->fetch_object())
			{
				print("<br>");
				printf ("%s<br>", $user->id);
				printf ("User First Name: %s<br>", $user->firstName);
				printf ("User Lase Name: %s<br>", $user->lastName);
				printf ("User Mobile: %s<br>", $user->mobile);
				printf ("User E-mail: %s<br>", $user->email);
				printf ("Is User Student: %s<br>", $user->isStudent);
				printf ("Is User Resident: %s<br>", $user->isResident);
				printf ("Is User Admin: %s<br>", $user->isUserAdmin);
				printf ("Is User used test day: %s<br>", $user->usedTestDay);
				printf ("User registration date: %s<br>", $user->registrationDate);
				
				$userJSON = json_encode($user, JSON_UNESCAPED_UNICODE);
				$funalStart .= $userJSON;
				$i++;
				
				if($i < $usersCount)
				{
					$funalStart .= ',';
				}
			}
			
			$finalEnd = $funalStart . ']}';
			
			echo $finalEnd;
			
		    $result->close();
		}
	}
	
	GetAllUsers($mysqli);
    
    $mysqli->close();
	
?>